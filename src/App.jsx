//import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Route, Routes } from 'react-router-dom';
import { Home } from './pages/Home';
import { Personaje } from './pages/Personaje';
import { Header } from './components/Navbar/Header';
import { Footer } from './components/Footer/Footer';
import './App.css'

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/personaje/:personajeId" element={<Personaje />} />
      </Routes>
      <Footer />
    </div>
  )
}

export default App
