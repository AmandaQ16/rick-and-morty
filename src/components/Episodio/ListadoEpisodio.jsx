import { EpisodioItem } from './EpisodioItem';

export function ListadoEpisodio({ episodios }) {
    return (
        <div className='row'>
            {episodios.map((episodio) => {
                let { id } = episodio;
                return <EpisodioItem {...episodio} key={id} />;
            })}
        </div>
    );
}
