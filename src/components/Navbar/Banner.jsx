import banner from '../../assets/img/rick-and-morty.png';

export function Banner() {
    return (
        <div>
            <img
                style={{ height: '756px', objectFit: 'cover' }}
                src={banner}
                className="card-img"
                alt="banner" />
        </div>);
}