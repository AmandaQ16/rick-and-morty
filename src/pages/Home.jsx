import { Banner } from "../components/Navbar/Banner";
import { Buscador } from "../components/Buscador/Buscador";
import { ListadoPersonajes } from "../components/Personajes/ListadoPersonaje";
import { useState } from "react";

export function Home() {
    let [buscador, setBuscador] = useState('');

    return (
        <div>
            <Banner />
            <div className="py-5">
                <h1 className="py-4" style={{color: 'white'}}>Personajes Destacados</h1>
                <Buscador valor={buscador} onBuscar={setBuscador} />
                <ListadoPersonajes buscar={buscador} />
            </div>
        </div>
    );
}