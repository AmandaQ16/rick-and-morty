import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
//import { ListadoEpisodio } from '../components/Episodio/ListadoEpisodio';
import { BannerDetallePersonaje } from "../components/Personajes/BannerDetallePersonaje";
import { ListadoEpisodio } from "../components/Episodio/ListadoEpisodio";

export function Personaje() {
    let { personajeId } = useParams();
    let [personaje, setPersonaje] = useState(null);
    let [episodios, setEpisodios] = useState(null);

    useEffect(() => {
        axios.get(`https://rickandmortyapi.com/api/character/${personajeId}`).then(respuesta => {
            setPersonaje(respuesta.data);
            let { episode } = respuesta.data;

            let peticionesEpisodios = episode.map((urlEpisodio) =>
                axios.get(urlEpisodio)
            );

            Promise.all(peticionesEpisodios).then((respuestaEpisodios) => {
                let datosFormateados = respuestaEpisodios.map(
                    (episodio) => episodio.data
                );
                setEpisodios(datosFormateados);
            });
        })
    }, []);

    return (
        <div className="p-5">
            {personaje ? (
                <div>
                    <BannerDetallePersonaje {...personaje} />
                    <h2 className="py-5" style={{ color: 'white' }}>Episodios</h2>
                    {episodios ? <ListadoEpisodio episodios={episodios} /> :
                        <div>Cargando Episodios...</div>
                    }
                </div>
            ) : (<h1>Cargando...</h1>)}
        </div>
    );
} 